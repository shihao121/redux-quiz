1. fetch data from backend
2. extract note intro component
3. add home page css
4. extract note detail component
5. render note intro to index html
6. add router to note details
7. complete home page css 
8. add delete note component
9. add delete function
10. add delete and back function
11. create new note page and router
12. create add note action 
13. add the new note page and home page style
14. add react icons for page