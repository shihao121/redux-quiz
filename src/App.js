import React, {Component} from 'react';
import './App.less';
import HomePage from "./page/HomePage";
import {BrowserRouter as Router} from "react-router-dom";
import {Route, Switch} from "react-router";
import NoteDetailsPage from "./page/NoteDetailsPage";
import NewNotePage from "./page/NewNotePage";

class App extends Component {
  render() {
    return (
      <div className='App'>
        <Router>
          <Switch>
            <Route exact path="/" component={HomePage}/>
            <Route path='/note/:id' component={NoteDetailsPage}/>
            <Route path='/notes/create' component={NewNotePage}/>
          </Switch>
        </Router>
      </div>
    );
  }
}

export default App;