const noteReducer = (state = {}, action) => {
  if (action.type === "GET_NOTE_DATA") {
    return {
      ...state,
      noteData: action.payload
    }
  } else if(action.type === "DELETE_NOTE") {
    return {
      ...state,
      deleteState: action.deleteState
    }
  } else if (action.type === "ADD_NOTE") {
    return {
      ...state,
      submitState: action.submitState
    }
  } else if (action.type === "CHANGE_TITLE") {
    return {
      ...state,
      title: action.title
    }
  } else if (action.type === "CHANGE_DESCRIPTION") {
    return {
      ...state,
      description: action.description
    }
  } else {
    return state;
  }
};

export default noteReducer;