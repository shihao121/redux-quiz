function deleteNote(id) {
  return (dispatch) => {
    fetch(`http://localhost:8080/api/posts/${id}`, {
      method: "DELETE",
    }).then(response => {
      if (response.status === 200) {
        dispatch({
          type: "DELETE_NOTE",
          deleteState: true
        })
      } else {
        return response.json();
      }
    }).then(error => {
      dispatch({
        type: "DELETE_NOTE",
        deleteState: false
      })
    })
  }
}

export default deleteNote;