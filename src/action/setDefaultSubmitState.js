const setDefaultSubmitState = () => ({
  type: "ADD_NOTE",
  submitState: false
});

export default setDefaultSubmitState