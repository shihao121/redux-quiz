const changeDescription = (description) => ({
  type: "CHANGE_DESCRIPTION",
  description: description
});

export default changeDescription