function getNoteData() {
  return (dispatch) => {
    fetch("http://localhost:8080/api/posts", {
      method: "GET",
    }).then(response => response.json()).then(result => {
      dispatch({
        type: "GET_NOTE_DATA",
        payload: result
      })
    })
  }

}

export default getNoteData;