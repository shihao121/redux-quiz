const changeTitle = (title) => ({
  type: "CHANGE_TITLE",
  title: title
});

export default changeTitle