const addNote = (title, description) => {
  return (dispatch) => {
    fetch("http://localhost:8080/api/posts", {
      method: "POST",
      body: JSON.stringify({title, description}),
      headers: {
        'content-type': 'application/json'
      }
    }).then(response => {
      if (response.status === 201) {
        dispatch({
          type: "ADD_NOTE",
          submitState: true
        })
      } else {
        dispatch({
          type: "ADD_NOTE",
          submitState: false
        })
      }
    })
  }
};

export default addNote;