import React, {Component} from 'react';
import deleteNote from "../action/deleteNote";
import {connect} from "react-redux";
import {Redirect} from "react-router";
import {Link} from "react-router-dom";

class DeleteNote extends Component {

  constructor(props) {
    super(props);
    this.deleteNote = this.deleteNote.bind(this);
  }

  render() {
    {if (this.props.deleteState) {
      return <Redirect exact to="/" />
    } else {
      return (
        <div className='delete-back'>
          <button onClick={this.deleteNote} className='delete'>删除</button>
          <button><Link to="/">返回</Link></button>
        </div>
      );
    }}
  }

  deleteNote() {
    this.props.deleteNote(this.props.id)
  }
}

const mapStateToProps = ({noteReducer: noteReducer}) => {
  return {
    deleteState: noteReducer.deleteState
  }
};

const mapDispatchToProps = (dispatch) => ({
  deleteNote: (id) => (dispatch(deleteNote(id)))
});

export default connect(mapStateToProps, mapDispatchToProps)(DeleteNote);