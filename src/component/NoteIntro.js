import React, {Component} from 'react';
import {Link} from "react-router-dom";

class NoteIntro extends Component {

  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div className="noteIntro">
        <Link to={{
          pathname: `/note/${this.props.id}`,
          title: this.props.title,
          description: this.props.description
        }}>{this.props.title}</Link>
      </div>
    );
  }
}

export default NoteIntro;