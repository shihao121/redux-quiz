import React, {Component} from 'react';
import {Link, Redirect} from "react-router-dom";
import addNote from "../action/addNote";
import changeTitle from "../action/changeTitle";
import changeDescription from "../action/changeDescription";
import {connect} from "react-redux";
import setDefaultSubmitState from "../action/setDefaultSubmitState";

class NewNoteForm extends Component {

  constructor(props) {
    super(props);
    this.submitNote = this.submitNote.bind(this);
  }

  componentDidUpdate() {
    this.props.setDefaultSubmitState();
  }

  render() {
    if (this.props.submitState) {
      return <Redirect exact to="/"/>
    } else {
      return (
        <div>
          <form className='submit-form'>
            <label><h2>标题</h2><input required="required" className="input-title" onChange={(event) =>
              this.props.changeTitle(event.target.value)}/></label>
            <label><h2>正文</h2><textarea className="input-description" required="required" onChange={(event) =>
              this.props.changeDescription(event.target.value)}>
          </textarea></label><br/>
            <div className='note-input-button'>
              <label>
                <button className="submit-button" onClick={this.submitNote}>提交</button>
              </label>
              <label>
                <button><Link to="/">取消</Link></button>
              </label>
            </div>
          </form>
        </div>
      );

    }
  }

  submitNote(event) {
    event.preventDefault();
    this.props.submitNote({title: this.props.title, description: this.props.description})
  }
}

const mapStateToProps = ({noteReducer}) => ({
  title: noteReducer.title,
  description: noteReducer.description,
  submitState: noteReducer.submitState
});

const mapDispatchToProps = (dispatch) => ({
  submitNote: ({title, description}) => (dispatch(addNote(title, description))),
  changeTitle: (title) => (dispatch(changeTitle(title))),
  changeDescription: (description) => (dispatch(changeDescription(description))),
  setDefaultSubmitState: () => (dispatch(setDefaultSubmitState()))
});

export default connect(mapStateToProps, mapDispatchToProps)(NewNoteForm);