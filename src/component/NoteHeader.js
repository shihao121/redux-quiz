import React from "react";
import { MdEventNote } from "react-icons/md";

const NoteHeader = () => {
  return (
    <header className='note-header'>
      <MdEventNote size={'40'}/>
      <p className="note-logo">NOTE</p>
    </header>
  )
};

export default NoteHeader