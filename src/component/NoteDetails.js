import React, {Component} from 'react';
import DeleteNote from "./DeleteNote";

class NoteDetails extends Component {

  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div className="noteDetails">
        <ul>
          <li>id: {this.props.match.params.id}</li>
          <li>title: {this.props.location.title}</li>
          <li>description: {this.props.location.description}</li>
        </ul>

        <hr />

        <DeleteNote id={this.props.match.params.id}/>
      </div>
    );
  }
}

export default NoteDetails;