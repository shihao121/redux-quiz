import React, {Component} from 'react';
import NoteHeader from "../component/NoteHeader";
import {Link} from "react-router-dom";
import NewNoteForm from "../component/NewNoteForm";

class NewNotePage extends Component {
  render() {
    return (
      <div className="input-note-page">
        <NoteHeader/>
        <h1>创建笔记</h1>
        <hr />
        <NewNoteForm submitState={false} />
      </div>
    );
  }
}

export default NewNotePage;