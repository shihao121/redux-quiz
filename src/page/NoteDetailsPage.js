import React, {Component} from 'react';
import {Link, Route} from "react-router-dom";
import getNoteData from "../action/getNoteData";
import {connect} from "react-redux";
import NoteDetails from "../component/NoteDetails";
import NoteHeader from "../component/NoteHeader";

class NoteDetailsPage extends Component {

  constructor(props) {
    super(props);
  }

  componentDidMount() {
    console.log("NoteDetailsPage did mount");
    this.props.getNoteData()
  }

  render() {
    return (
      <div className="noteDetailsPage">
        <NoteHeader/>
        <ul className="noteNav">
          {this.props.noteData !== undefined ? Object.values(this.props.noteData).map(note => {
            return (<li key={note.id}><Link to={{
              pathname: `/note/${note.id}`,
              title: `${note.title}`,
              description: `${note.description}`
            }}>{note.title}</Link></li>)
          }) : ""}
        </ul>
        <Route path="/note/:id" component={NoteDetails}/>
      </div>
    );
  }

}

const mapStateToProps = ({noteReducer}) => {
  return {noteData: noteReducer.noteData}
};

const mapDispatchToProps = (dispatch) => ({
  getNoteData: () => (dispatch(getNoteData()))

});


export default connect(mapStateToProps, mapDispatchToProps)(NoteDetailsPage);