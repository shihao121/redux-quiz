import React, {Component} from 'react';
import getNoteData from "../action/getNoteData";
import {connect} from "react-redux";
import NoteIntro from "../component/NoteIntro";
import NoteHeader from "../component/NoteHeader";
import {Link} from "react-router-dom";
import {MdFilter1} from "react-icons/md";

class HomePage extends Component {

  constructor(props) {
    super(props);
  }

  componentDidMount() {
    this.props.getNoteData();
  }

  render() {
    return (
      <div>
        <NoteHeader />
        <div className="noteIntros">
          {this.props.noteData !== undefined ? this.props.noteData.map(note => {
            return (<NoteIntro key={note.id} title={note.title} id={note.id} description={note.description}/>)
          }) : ""}
          <div className="noteIntro">
            <Link to="/notes/create">
              <MdFilter1 size='40'/>
            </Link>
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = ({noteReducer}) => {
  return {noteData: noteReducer.noteData}
};

const mapDispatchToProps = (dispatch) => ({
  getNoteData: () => (dispatch(getNoteData()))
});

export default connect(mapStateToProps, mapDispatchToProps)(HomePage);